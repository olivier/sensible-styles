%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fluid Mech Notes
% A stylesheet assembled for the purpose of writing
% lecture notes in Fluid Mechanics, suited for 
% scientific documents, designed to be rapidly 
% customized.
% Released as CC-0
% from https://framagit.org/olivier/sensible-styles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main dependencies
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{fontsandencoding}
\usepackage{uselibertine}

\usepackage{itsinenglish}

\usepackage{troveofcommonpackages}

\usepackage{cleveref}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Document layout
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[a4paper,
			top=			1cm,
			bottom=			1cm,
			includemp,
			headheight=		15pt,
			headsep=		-15pt,
			marginparwidth=	3.65cm,
			marginparsep=	0.35cm,
			inner=			3cm,
			outer=			1cm,
			centering,
			bindingoffset=	0cm,
		   ]{geometry}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main appearance settings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\linespread{1.0}	% standard line spacing

% Control spacing above/below figures tightly
	\setlength{\textfloatsep}{20pt plus 0pt minus 0pt} 	% between a float (tables or figures) and text, LaTeX-positionned floats
	\setlength{\floatsep}{20pt plus 10pt minus 0pt} 	% between two floats (either on top or at bottom of page), LaTeX-positionned floats

% Make figure placement less strict with regards to text
	\renewcommand{\topfraction}{.85}
	\renewcommand{\bottomfraction}{.7}
	\renewcommand{\textfraction}{.15}
	\renewcommand{\floatpagefraction}{.66}
	\renewcommand{\dbltopfraction}{.66}
	\renewcommand{\dblfloatpagefraction}{.66}
	\setcounter{topnumber}{9}
	\setcounter{bottomnumber}{9}
	\setcounter{totalnumber}{20}
	\setcounter{dbltopnumber}{9}

\raggedbottom	% do not change inter-paragraph spacings to maintain contsant page text height

\thickmuskip=5mu minus 1mu

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of content
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Tocloft allows control of Table of Contents format. Use option [subfigure] to prevent clash with subfigure package
\usepackage[]{tocloft}			
	\renewcommand{\cftdot}{} 				% remove dots
	\setcounter{tocdepth}{1} 				% depth of ToC
	\setlength{\cftbeforechapskip}{0.29cm} 	% space before chapters in ToC
	\setlength{\cftbeforesecskip}{0.3cm}	% space before sections in ToC
	\setlength{\cftsecindent}{1.25cm}		% indent before sections in ToC
	\setlength{\cftsecnumwidth}{3em}			% length of number width in ToC for \section
	\setlength{\cftsubsecnumwidth}{3em}		% length of number width in ToC for \subsection
	%\setlength{\cftsubsubsecnumwidth}{4em}	% length of number width in ToC for \subsubsection
\renewcommand{\cfttoctitlefont}{\hfill\huge}
\renewcommand{\cftaftertoctitle}{\hfill}
\setlength{\cftbeforetoctitleskip}{0cm}
\setlength{\cftaftertoctitleskip}{0cm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Numbering and titles
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize dummy values for chapter title and number
\newcommand{\numberofthischapter}{1}
\newcommand{\titleofthischapter}{Title}

\usepackage{enumitem}		% Control margins in enumerate lists

\usepackage{titlesec} 		% Enables customization of section/subsection titles with \titleformat
%\titleformat{ command }[ shape ]{ format }{ label }{ sep }{ before-code }[ after-code ]

%%% Sections & subsections
	% Command to restore the section style, used after every exercise sheet
	\renewcommand{\thefigure}{\numberofthischapter.\arabic{section}}
	\newcommand{\restoredefaultsectionstyle}{\titleformat{\section}{\Large\raggedright}{\thesection}{1em}{}[\vspace{0.1em}\titlerule]}
		\restoredefaultsectionstyle		% initialize (Use the default section style at the start of the document)

	\titleformat{\subsection}{\clearfloats\large\bfseries}{\thesubsection}{1em}{}
	
		% Enable numbering of sub-sub-sections etc. down to level 4 (by default the 'book' class does not number things this deep)
		\setcounter{secnumdepth}{4} 		
		\titleformat{\subsubsection}{\clearfloats\Large\titlerule\vspace{0.1em}}{\thesubsubsection}{1em}{}[]
		% Exercises in exercise sheets are subsubsections, but we cheat with the numbering and number them as if they were subsections:
		\renewcommand\thesubsubsection{\thechapter.\arabic{subsubsection}}

%%% Other numberings
	% Numbering for tables and figures
	\renewcommand{\thefigure}{\numberofthischapter.\arabic{figure}}
	\renewcommand{\thetable}{\numberofthischapter.\arabic{table}}

	% Numbering for equations
	\renewcommand{\theequation}{\numberofthischapter/\arabic{equation}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Chapter page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% mini ToC (mini table of contents, printed at the start of each chapter)
\usepackage[tight, undotted]{minitoc}
	\nomtcrule 													% no horizontal lines
	\renewcommand{\mtctitle}{} 									% no "contents" title
	\renewcommand*\l@section{\@dottedtocline{1}{0em}{2.3em}}	% voodoo
	\renewcommand*\l@subsection{\@dottedtocline{2}{2.6em}{3em}}	% voodoo
	\renewcommand{\mtcgapbeforeheads}{200pt}					% voodoo


	% Shortcut to insert minitoc in main .tex file
	\newcommand{\uneminitoc}{%
		%\linespread{1.2}% increase line spacing
		\hypersetup{linkcolor=black}% In the minitoc, links should be black
		{\small\minitoc}%
		\hypersetup{linkcolor=internallinkcolor}% Restore normal link color after the minitoc
		%\linespread{1.0}% back to original value
	}

% Do not break double page when we start a new chapter;
% instead, \cleardoublepage is called at the *end* of each
% new chapter. This enables the creation of individual chapter PDFs
% that do not start with blank pages:
\newcommand\chapterbreak{}

\renewcommand\chapter{\secdef\@chapter\@schapter} % Barebone \chapter definition, that allows either \chapter{} or \chapter*{}

\titleformat{\chapter}[display]			% \titleformat{commandtobeformatted}[shape]
  {\vspace{-9em}\center \color{black}}	% {before-code (format)}
  {}									% {label}
  {-0.2em}								% {separation}
  {\Large Fluid Dynamics\\ \chaptertitlename~\numberofthischapter~-- \Large}	% {between-label-and-text-code}
  [{\footnotesize last edited \lasteditdate\\
  	by Olivier Cleynen — \href{https://fluidmech.ninja/}{https://fluidmech.ninja/} \vspace{-4em}}]						% [after text code]
  
\newcommand{\fluidmechchaptertitle}{%
	\chapter{\titleofthischapter}%
	\uneminitoc
}

% Code inserted at the end of every chapter notes, before exercise sheet begins
\newcommand{\atendofchapternotes}{%
		\cleardoublepage% Clear pages till new odd page 
	}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Style changes in exercise sheets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\fluidmechexercisestitle}{%
	\section[Problems]{\titleofthischapter}%
	}

% Code inserted at the start of every exercise sheet
\newcommand{\atstartofexercises}{%
	% Shrink margins: exercises sheets are often printed out, and we do not encourage note-taking as much
	\newgeometry{top=1.5cm, bottom=1cm, includefoot, left=3cm, right=3cm}
	
	% Redefine section to new style. This will be cancelled out with a \restoredefaultsectionstyle at the end of the sheet.
	\titleformat{\section}%
		{\Large\centering}%{ before-code (format) }
		{}%{ label }
		{0cm}%{ sep }
		{\Large Problem sheet \numberofthischapter:~ \Large}%{ between-code }
		[{\footnotesize last edited \lasteditdate\\
		by Olivier Cleynen — \href{https://fluidmech.ninja/}{https://fluidmech.ninja/}}]%[ after-code ]	

	\setlength{\fancyfootoffset}{1cm}%

	% Numbering for questions in problems (which are subsubsections)
	\renewcommand\theenumi{\textcolor{unitcolor}{\thesubsubsection.}\arabic{enumi}}
	% Push enumerate items (problem questions) further to right to make room for longer item number
	\setlist[enumerate,1]{leftmargin=1.5cm}
}

% Code inserted in exercise sheet, at start of answer section
\newcommand{\startofanswers}{%
	% Numbering for answers
	%\setcounter{enumi}{0}
	%\setcounter{subsubsection}{0}
	\renewcommand\theenumi{\textcolor{unitcolor}{\numberofthischapter.}\arabic{enumi}}
	\renewcommand\labelenumi{\theenumi}
	\renewcommand\theenumii{\textcolor{unitcolor}{\theenumi.}\arabic{enumii}}
	\renewcommand\labelenumii{\theenumii}
}

% Code inserted at the end of every exercise sheet
\newcommand{\atendofexercises}{%
		\renewcommand\theenumi{\arabic{enumi}}% reset enumerate item number
		\renewcommand\theenumii{\arabic{enumii}}% reset enumerate subitem number
		\renewcommand\labelenumi{\theenumi.}
		\setlist[enumerate,1]{leftmargin=\labelwidth+\labelsep}% reset enumerate margins
		\clearpage\restoredefaultfootoffset%
		\restoregeometry%
		\restoredefaultsectionstyle% back to default section style
		\cleardoublepage% We are at the end of the chapter: clear pages to new odd page (see \chapterbreak{} comment above)
	}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Misc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{changepage}
	% allow ad-hoc changing of margins (overflowing equations)
	% with 	\begin{adjustwidth}{-2cm}{-2cm} CONTENT \end{adjustwidth}

\usepackage{pdfpages} % enable \includepdf{} to embed external PDFs


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Attribution of references
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% see also \mecafluboxen in texttemplates-fluidmech.sty

% Attribution/source/licensing for exercises
	\renewcommand{\wherefrom}[1]{\nopagebreak\begin{flushright}\footnotesize\vspace{-2em}\textit{#1}\vspace{-1.5em}\end{flushright}\nopagebreak}

% Point to interesting references in lecture note text
	\newcommand{\coveredin}[1]{\nopagebreak\begin{flushright}\scriptsize\vspace{-1em}\textit{This topic is well covered in #1}\vspace{-1.5em}\end{flushright}\nopagebreak}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Headers and footers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\pagestyle{fancy}
\lhead{}
\chead{}
\rhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\defaultfooter}{\fancyfoot[LE,RO]{\vspace{-1.4cm}\thepage}\cfoot{\footertextforthisdocument}}
	\defaultfooter % initialize
\newcommand{\fancyfootdefaultvalue}{3cm}
\newcommand{\restoredefaultfootoffset}{\fancyfootoffset[LE,RO]{\fancyfootdefaultvalue}}
	\restoredefaultfootoffset % initialize


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Bibliography
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Point to .bib file; this can be a symlink to a more complex path
\addbibresource{/home/olivier/.fluidmechbib.bib}
\addbibresource{/home/olivier/.humanbib.bib}
\addbibresource{/home/olivier/.fluidmechbouquet.bib}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Final PDF set-up and late package calls
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\hypersetup{%
	pdftex,
	pdfauthor={\authorofthisdocument},
	pdftitle={\titleofthisdocument},
	pdfsubject={},
	pdfkeywords={\keywordsofthisdocument},
	pdfnewwindow=false,				% open links in new window?
	colorlinks=true,				% false: boxed links (why??); true: colored links
	linkcolor=internallinkcolor,	% color for internal links
	linkbordercolor=white,
	citecolor=internallinkcolor,
	citebordercolor=white,
	filecolor=black,
	filebordercolor=white,
	urlcolor=externallinkcolor,		% color for external links
	bookmarks=true,					% show bookmarks bar
	unicode=false,					% non-Latin characters in Acrobat’s bookmarks
	pdftoolbar=true,				% show Acrobat’s toolbar
	pdfmenubar=true,				% show Acrobat’s menu
}

\usepackage[all]{hypcap}	% Make links to figures point to the image instead of the caption. Must be loaded after hyperref. Broken when caption{} is redefined. Also, needs a paragraph before it is called.
