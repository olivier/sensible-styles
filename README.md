Sensible LaTeX Styles
=====================

A set of modular LaTeX stylesheets designed to produce paper documents and presentation slides.

These stylesheets are 

* Modular: font or language settings can be swapped easily, while all stylesheets share a common core of packages;
* Easily customizable: extensive comments and clear structure make customization possible for LaTeX beginners;
* Easy to use: code shorthands are provided to minimize human time and code input while typing.

These characteristics have been preferred over speed and aesthetics: documents may compile slowly and contain typographic or layout imperfections.

## Project structure

* the `0 root styles` folder contains low-level modules;
* the `1 basic styles` folder contains directly-usable, very simple stylesheets:
    * `letter_english.sty` and `letter_french.sty` for very simple text documents;
    * `notes_english.sty` for a slightly more capable document;
    * `slides_english.sty` and `slides_french.sty` for no-nonsense, simple, robust presentation slides.
* the `2 special purpose styles` folder contains more stylesheets designed for special purposes:
	* `magdeburg` produces minimalist, wide-margin rich documents;
	* `fluidmech` powers the lecture notes at <a href="http://fluidmech.ariadacapo.net">fluidmech.ariadacapo.net</a>;
	* `thermodynamique` powers the lecture notes at <a href="http://thermodynamique.ninja/">thermodynamique.ninja</a>.

For convenience,

* `demo` contains demonstration PDFs to showcase the main styles, and;
* `templates` contains ready-to-use `.tex` documents.


## Installation

These stylesheets were developed and are tested on a <a href="https://www.debian.org/">Debian</a> 8 “Jessie” GNU/Linux platform. They require:

* Every `texlive-*` Debian software package installed from the repositories;
* The `libertine-legacy` LaTeX font package (<a href="https://tex.stackexchange.com/questions/260535/how-can-i-use-the-slanted-variant-of-the-libertine-font">thus enabling the slanted variant of that font</a>);
* A recent version of <a href="http://ctan.org/pkg/datetime2">the `datetime2` LaTeX package</a>.

Once these requirements are met, to install, simply clone this repository, i.e.

    git clone https://git.framasoft.org/olivier/sensible-styles.git

Then <a href="https://tex.stackexchange.com/questions/1137/where-do-i-place-my-own-sty-or-cls-files-to-make-them-available-to-all-my-te/1142#1142">copy those files into your `TEXINPUTS` folder</a>. The LaTeX packages should be immediately ready to use like so:

    pdflatex magdeburg_demo.tex


## Author & licensing

These stylesheets are written by <a href="http://ariadacapo.net/">Olivier Cleynen</a> and are released into the public domain with the <a href="https://creativecommons.org/publicdomain/zero/1.0/deed.en">CC-0</a> dedication. No warranty of any kind is associated to the use of these tools.
